﻿using System;

public class Result
{
    private string name;
    private double[] min, max;
    private enum Limits
    {
        MIN = 0,
        MAX = 100
    }
    public Result(string name, double? minMale, double? minFemale, double? maxMale, double? maxFemale)
    {
        // Set the name
        this.name = name;
        // Set the min and max from limits if null
        minMale = minMale.HasValue ? minMale.Value : (double)Limits.MIN;
        minFemale = minFemale.HasValue ? minFemale.Value : (double)Limits.MIN;
        maxMale = maxMale.HasValue ? maxMale.Value : (double)Limits.MIN;
        maxFemale = maxFemale.HasValue ? maxFemale.Value : (double)Limits.MIN;
        // Set the min max from the values
        this.min = new double[] { (double)minMale, (double)minFemale };
        this.max = new double[] { (double)maxMale, (double)maxFemale };
    }
    public bool isInRange(int gender, double bmi)
    {
        return bmi >= min[gender] && bmi < max[gender];

    }
    public string getName()
    {
        return this.name;
    }
}
