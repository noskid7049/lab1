﻿using System;
using System.Collections;

public class ResultCalculator
{
    private ArrayList resultList = new ArrayList();
    public ResultCalculator()
    {
        // Add the available results along with the BMI limits
        resultList.Add(new Result("anorexia", null, null, 17.5, 17.5));
        resultList.Add(new Result("underweight ", null, null, 20.7, 19.1));
        resultList.Add(new Result("in normal range", 20.7, 19.1, 26.4, 25.8));
        resultList.Add(new Result("marginally overweight", 26.4, 25.8, 27.8, 27.3));
        resultList.Add(new Result("overweight", 27.8, 27.3, 31.1, 32.3));
        resultList.Add(new Result("very overweight or obese", 31.1, 32.3, 35, 35));
        resultList.Add(new Result("severely obese", 35, 35, 40, 40));
        resultList.Add(new Result("morbidly obese", 40, 40, 50, 50));
        resultList.Add(new Result("super obese", 50, 50, 60, 60));
    }
    public ArrayList getResultList()
    {
        return resultList;
    }
    public string getName(int gender, double bmi)
    {
        string name = "";
        // Check if the given BMI matches the results in our list
        // If yes, return the result
        for (int i = 0; i < resultList.Count; i++)
        {
            Result result = (Result)resultList[i];
            // Check if the calculated BMI is in range of the current Result
            if (result.isInRange(gender, bmi)){
                // Get the name of the Result
                name = result.getName();
                break;
            }
        }
        return name;
    }
    
}
