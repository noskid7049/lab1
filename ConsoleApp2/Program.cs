﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace ConsoleApp2
{

    class Program
    {
        static void Main(string[] args)
        {
            string cont = "Y";
            do
            {
                runApp();
                // Ask if the app should be re-run
                Console.Write("Continue? ");
                cont = Console.ReadLine();
             // Continue until the user inputs "y" || "Y"
            } while (cont.ToLower() == "y");

        }
        static void runApp()
        {
            string input;
            string name;
            int gender;
            double weight, height;

            ResultCalculator calc = new ResultCalculator();
            // Get the required details from the input
            Console.WriteLine("Details: ");

            Console.Write("Gender (M/F): ");
            input = Console.ReadLine();
            gender = (input == "m" || input == "M") ? (int)Gender.Male : (int)Gender.Female;

            Console.Write("Name: ");
            name = Console.ReadLine();

            Console.Write("Weight in Pounds: ");
            weight = Single.Parse(Console.ReadLine());

            Console.Write("Height in Inches: ");
            height = Single.Parse(Console.ReadLine());

            double BMI = (weight / (height * height)) * 703;
            BMI = (double)(Math.Round((BMI * 100)) / 100);

            showMessage(name);

            Console.WriteLine("Your BMI is " + BMI);

            Console.WriteLine(calc.getName(gender, BMI));
        }
        // Task 1 implementation
        static void showMessage(string name)
        {
            MessageBox.Show(name, "Name Box",
         MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
